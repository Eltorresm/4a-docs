from django.conf import settings
from django.contrib.auth.models import Permission
from rest_framework import generics, serializers, status, views
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer

class UserDeleteView(views.APIView):
    def delete(self,request,*args,**kwargs):
        # queryset = User.objects.all()
        # serializer_class = UserSerializer
        # permission_classes = (IsAuthenticated,)

        # id_user_URL = kwargs['pk']
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        # if valid_data['user_id'] != id_user_URL:
        #     stringResponse = {'detail':'Unauthorized Request'}
        #     return Response(stringResponse,status=status.HTTP_401_UNAUTHORIZED)

        user = User.objects.filter(id=valid_data['user_id']).first()
        user.delete()
        stringResponse = {'detail':'User delete'}
        return Response(stringResponse)

