from django.conf import settings
from django.contrib.auth.models import Permission
from rest_framework import generics, serializers, status, views
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer

class UserUpdateView(views.APIView):
    def put(self,request,*args,**kwargs):
        queryset = User.objects.all()
        serializer_class = UserSerializer
        permission_classes = (IsAuthenticated,)

        id_user_body = request.data.pop("user_id")
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != id_user_body:
            stringResponse={'detail':'Unauthorized Request'}
            return Response(stringResponse,status=status.HTTP_401_UNAUTHORIZED)

        user = User.objects.filter(id=kwargs['pk']).first()
        serializer = UserSerializer(user,data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
        